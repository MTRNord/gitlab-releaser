# frozen_string_literal: true

require 'gitlab/releaser/version'

module GitlabReleaser
  class Error < StandardError; end
end
