# frozen_string_literal: true

require 'matrix_sdk'

# A filter to only discover joined rooms
ROOM_DISCOVERY_FILTER = {
    event_fields: %w[sender membership],
    presence: {senders: [], types: []},
    account_data: {senders: [], types: []},
    room: {
        ephemeral: {senders: [], types: []},
        state: {
            senders: [],
            types: %w[m.room.aliases m.room.canonical_alias m.room.member],
            lazy_load_members: true
        },
        timeline: {senders: [], types: []},
        account_data: {senders: [], types: []}
    }
}.freeze

# A filter to only retrieve messages from rooms
ROOM_STATE_FILTER = {
    presence: {senders: [], types: []},
    account_data: {senders: [], types: []},
    room: {
        ephemeral: {senders: [], types: []},
        state: {
            types: ['m.room.member'],
            lazy_load_members: true
        },
        timeline: {
            types: ['m.room.message']
        },
        account_data: {senders: [], types: []}
    }
}.freeze

module GitlabReleaser
  class MatrixClient < MatrixSdk::Client
    def initialize(hs_url, access_token)
      super hs_url, sync_filter_limit: 10
      api.access_token = access_token

      @pls = {}
      @tracked_rooms = []
      @filter = ROOM_STATE_FILTER.dup
    end

    def add_listener(room)
      room.on_event.add_handler { |ev| on_message(room, ev) }
      @tracked_rooms << room.id
    end

    def run
      # Only track messages from the listened rooms
      @filter[:room][:rooms] = @tracked_rooms
      start_listener_thread(filter: @filter.to_json)
    end

    private

    def get_user_level(room, mxid)
      levels = @pls[room.id] ||= api.get_power_levels(room.id)[:users]
      levels[mxid.to_sym]
    end

    def on_message(room, event)
      return unless event.type == 'm.room.message' || event[:content][:msgtype] == 'm.text'

      user = get_user event.sender
      admin_level = get_user_level(room, user.id) || 0
      prefix = ' '
      prefix = '+' if admin_level >= 50
      prefix = '@' if admin_level >= 100
      puts "[#{Time.now.strftime '%H:%M'}] <#{prefix}#{user.display_name}> #{event.content[:body]}"
    end
  end
end
