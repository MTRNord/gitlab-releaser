# frozen_string_literal: true

require_relative '../command'
require_relative '../../../../lib/gitlab/releaser/config'
require_relative '../../../../lib/gitlab/releaser/MatrixClient'

module GitlabReleaser
  module Commands
    # Main command to run this tool. This opens a listener to matrix
    class Start < GitlabReleaser::Command
      def initialize(options)
        @options = options
        @config = GitlabReleaser.config
        @matrix_client = GitlabReleaser::MatrixClient.new @config.matrix["homeserver"], @config.matrix["access_token"]
      end

      def execute(input: $stdin, output: $stdout)

        puts 'Warming up...'
        sync_filter = @matrix_client.sync_filter.merge(ROOM_DISCOVERY_FILTER)
        sync_filter[:room][:state][:senders] << @matrix_client.mxid
        begin
          @matrix_client.listen_for_events(timeout: 30, filter: sync_filter.to_json)
        rescue MatrixTimeoutError => e
          puts "Got timeout..."
          return
        end

        puts 'Finding action room...'
        room = @matrix_client.find_room(@config.matrix["action_room"])
        room ||= begin
          puts 'Joining action room...'
          @matrix_client.join_room(@config.matrix["action_room"])
        end

        @matrix_client.add_listener(room)

        puts 'Starting listener'
        @matrix_client.run

        puts 'Listening...'

        @matrix_client.on_error.add_handler do |err|
          if err.instance_of? StandardError
            puts "thread died. restarting client..."
            puts err.error
            @matrix_client.run
          end
        end
        loop do
          # Noop
          #unless @matrix_client.listening?
          #  puts "thread died. restarting client..."
          #  @matrix_client.run
          #end

          sleep 50
        rescue Interrupt
          puts 'Interrupted, exiting...'
        ensure
          # @matrix_client.logout if @matrix_client && @matrix_client.logged_in?
        end

      end
    end
  end
end
