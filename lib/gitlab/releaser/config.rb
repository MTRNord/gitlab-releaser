# frozen_string_literal: true

require 'anyway'

module GitlabReleaser
  def self.config
    @config ||= Config.new
  end

  class Config < Anyway::Config
    config_name :main
    attr_config gitlab: { endpoint: '', private_token: '' },
                matrix: { homeserver: '', access_token: '', action_room: '' }
  end
end
