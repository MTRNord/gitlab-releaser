# frozen_string_literal: true

require 'thor'

module GitlabReleaser
  # Handle the application command line parsing
  # and the dispatch to various command objects
  #
  # @api public
  class CLI < Thor
    # Error raised by this runner
    Error = Class.new(StandardError)

    desc 'version', 'gitlab-releaser version'
    def version
      require_relative 'version'
      puts "v#{GitlabReleaser::VERSION}"
    end
    map %w[--version -v] => :version

    desc 'start', 'Command description...'
    method_option :help, aliases: '-h', type: :boolean,
                         desc: 'Display usage information'
    def start(*)
      if options[:help]
        invoke :help, ['start']
      else
        require_relative 'commands/start'
        GitlabReleaser::Commands::Start.new(options).execute
      end
    end
  end
end
