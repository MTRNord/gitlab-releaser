# frozen_string_literal: true

RSpec.describe '`gitlab-releaser start` command', type: :cli do
  it 'executes `gitlab-releaser help start` command successfully' do
    output = `gitlab-releaser help start`
    expected_output = <<~OUT
      Usage:
        gitlab-releaser start

      Options:
        -h, [--help], [--no-help]  # Display usage information

      Command description...
    OUT

    expect(output).to eq(expected_output)
  end
end
