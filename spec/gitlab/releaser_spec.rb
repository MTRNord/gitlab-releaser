# frozen_string_literal: true

RSpec.describe GitlabReleaser do
  it 'has a version number' do
    expect(GitlabReleaser::VERSION).not_to be nil
  end
end
