[![Coverage report](https://gitlab.com/MTRNord/gitlab-releaser/badges/master/coverage.svg?job=test)](https://mtrnord.gitlab.io/gitlab-releaser)
# Gitlab::Releaser

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/gitlab/releaser`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab-releaser'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install gitlab-releaser

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/MTRNord/gitlab-releaser. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Code of Conduct

Everyone interacting in the Gitlab::Releaser project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/gitlab-releaser/blob/master/CODE_OF_CONDUCT.md).

## Copyright

Copyright (c) 2019 Marcel. See [AGPL-v3.0 License](LICENSE.txt) for further details.